from flask import Flask
import os
import socket

app = Flask(__name__)

@app.route("/")
def home():
    html = "<h3>Hello {name}!</h3>" \
            "<b>Hostname:</b> {hostname} <br/>"\
            "<b>Username: </b> {username} <br/>"

    return html.format(name = os.getenv("NAME"),\
            hostname = socket.gethostname(),\
            username = os.getenv("USER"))

if __name__ == "__main__":
    app.run(host = "0.0.0.0", port = 80)

# Numbers at the beginning are not part of the code.
